// Comment to get more information during initialization
logLevel := Level.Warn

// The Typesafe repository
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

// Use the Play sbt plugin for Play projects
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.4.3")

// Generates getters and setters for Java properties
addSbtPlugin("com.typesafe.sbt" % "sbt-play-enhancer" % "1.1.0")

// Enable native packaging options (see http://www.scala-sbt.org/sbt-native-packager/)
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.3")

// Plugin for publishing to bintray
addSbtPlugin("me.lessis" % "bintray-sbt" % "0.3.0")