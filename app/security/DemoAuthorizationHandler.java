package security;

import ch.insign.cms.security.CmsAuthorizationHandler;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.LoggerFactory;
import play.mvc.Http.Context;
import play.mvc.Result;


public class DemoAuthorizationHandler extends CmsAuthorizationHandler {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(DemoAuthorizationHandler.class);

    @Override
    public Result onUnauthorized(Context ctx, AuthorizationException e) {
        logger.debug("onUnauthorized event triggered");
	    return super.onUnauthorized(ctx, e);
    }

    @Override
    public Result onUnauthenticated(Context ctx, AuthorizationException e) {
        logger.debug("onUnauthorized event triggered");
        return super.onUnauthenticated(ctx, e);
    }

}
