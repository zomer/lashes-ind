package models;

import blocks.demopageblock.DemoPageBlock;
import blocks.teaserblock.TeaserBlock;
import ch.insign.cms.models.*;
import widgets.registeredusers.RegisteredUsersWidget;

public class MyCmsSetup  extends Setup {

    @Override
    public void registerPartyFormManager() {

        // register party form manager to specify user's custom edit and create forms
        CMS.setPartyFormManager(new MyUserHandler());
    }

    @Override
    public void registerPartyEventHandler() {

        // register party event handler handles party's CRUD events
        CMS.setPartyEvents(new MyUserHandler());
    }

    @Override
    public void registerBlocks() {
        super.registerBlocks();
        CMS.getBlockManager().register(
                DemoPageBlock.class,
                TeaserBlock.class
        );
    }

    @Override
    public void registerContentFilters() {
        super.registerContentFilters();
        CMS.getFilterManager().register(
                new RegisteredUsersWidget()
        );
    }

    @Override
    public void addExampleData() {
        super.addExampleData();
        createTeaserBlocks();
        createWidgetExampleBlock();
    }

    /**
     * Override helper method to create example pages of DemoPageBlock class
     */
    @Override
    protected PageBlock createPage(String key, PageBlock parent, String vpathEN) {
        DemoPageBlock page = new DemoPageBlock();
        page.setKey(key);
        if (parent != null) {
            parent.getSubBlocks().add(page);
            page.setParentBlock(parent);
        }
        page.save();

        createNavItem(page, vpathEN, "en");
        return page;
    }

    private void createWidgetExampleBlock() {
        PageBlock homepage = (PageBlock) AbstractBlock.find.byKey(KEY_HOMEPAGE);
        try {
            CollectionBlock sidebar = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "sidebar");
            ContentBlock contentBlock = (ContentBlock)sidebar.addSubBlock(ContentBlock.class);
            contentBlock.getTitle().set("en", "Widgets");
            contentBlock.getContent().set("en",
                    "<p>Content filters are a great tool to let the user add variables freely inside any content, " +
                    "e.g. [[currentUserCount]] which your filter resolves to the current value " +
                    "when displaying the page.</p>\n" +
                    "<h4>Content filter example.</h4>\n" +
                    "<p>Last registered users: [[registeredUsersWidget:5]]</p>\n" +
                    "<p><a href=\"https://confluence.insign.ch/display/PLAY/Play+CMS\">" +
                    "Learn more </a> about filter framework</p>");
            contentBlock.save();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createTeaserBlocks() {
        try {
            PageBlock homepage = (PageBlock) AbstractBlock.find.byKey(KEY_HOMEPAGE);
            CollectionBlock bottomPane = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "bottom");
            TeaserBlock block, block2, block3;
            block = (TeaserBlock)bottomPane.addSubBlock(TeaserBlock.class);
            block.getTitle().set("en", "Teaser block");
            block.getSubtitle().set("en", "Lorep Ipsum");
            block.getContent().set("en", "This is an example of customizing cms content blocks");
            block.getLogoUrl().set("en", "/assets/images/yacht.png");
            block.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block.getLinkText().set("en", "Learn more");

            block2 = (TeaserBlock)bottomPane.addSubBlock(TeaserBlock.class);
            block2.getTitle().set("en", "Teaser block");
            block2.getSubtitle().set("en", "Lorep Ipsum");
            block2.getContent().set("en", "This is an example of customizing cms content blocks");
            block2.getLogoUrl().set("en", "/assets/images/yacht2.jpg");
            block2.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block2.getLinkText().set("en", "Learn more");

            block3 = (TeaserBlock) bottomPane.addSubBlock(TeaserBlock.class);
            block3.getTitle().set("en", "Teaser block");
            block3.getSubtitle().set("en", "Lorep Ipsum");
            block3.getContent().set("en", "This is an example of customizing cms content blocks");
            block3.getLogoUrl().set("en", "/assets/images/yacht3.jpg");
            block3.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block3.getLinkText().set("en", "Learn more");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
