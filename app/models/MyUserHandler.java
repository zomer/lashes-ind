package models;

import ch.insign.cms.models.PartyEvents;
import ch.insign.cms.models.PartyFormManager;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.playauth.party.support.DefaultParty;
import play.twirl.api.Html;
import play.data.Form;
import play.data.validation.ValidationError;

import java.util.List;

/**
 * MyUserHandler class specify user's custom edit and create forms
 * and handles custom user's CRUD events
 */
public class MyUserHandler implements PartyFormManager, PartyEvents {

    @Override
    public Html editForm(Form editForm, DefaultParty party) {
        return views.html.admin.user.editForm.render(new AdminContext(), editForm, (User)party);
    }

    @Override
    public Html createForm(Form form) {
        return views.html.admin.user.createForm.render(form);
    }

    @Override
    public void onCreate(Form form, DefaultParty party) {
        User user = (User) party;
        User userForm = ((Form<User>) form).get();

        user.setLastName(userForm.getLastName());
        user.setPhone(userForm.getPhone());
    }

    @Override
    public void onUpdate(Form form, DefaultParty party) {

    }

    @Override
    public void onDelete(DefaultParty party) {

    }
}
