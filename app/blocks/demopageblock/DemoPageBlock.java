package blocks.demopageblock;

import blocks.demopageblock.html.edit;
import blocks.demopageblock.html.demoPageBlock;
import ch.insign.cms.models.BlockCache;
import ch.insign.cms.models.PageBlock;
import ch.insign.cms.models.Setup;
import play.twirl.api.Html;
import play.data.Form;
import play.mvc.Controller;

import javax.persistence.*;

@Entity
@Table(name = "demo_page_block")
@DiscriminatorValue("DemoPageBlock")
public class DemoPageBlock extends PageBlock {

    @Override
    public Html render() {
        return demoPageBlock.render(this);

    }

    public Html editForm(Form editForm) {
        return edit.render(this, editForm, Controller.request().getQueryString("backURL"), null);
    }

	/**
	 * Example of using a custom BlockCache configuration
	 */
	@Override
	public BlockCache cache() {
		if (cache == null) {
			cache = new BlockCache(this) {
				/**
				 * Determines if caching should be used (according to the cache configuration and request)
				 * (use isCached() to check if a cached version is available)
				 */
				@Override
				public boolean useCache() {

					// In this example, do not cache the (entire) homepage
					if (Setup.KEY_HOMEPAGE.equals(block.getKey())) {
						return false;
					} else {
						return super.useCache();
					}
				}
			};
		}
		return cache;
	}
}
