package widgets.registeredusers;

import ch.insign.cms.models.FrontendWidget;
import ch.insign.commons.filter.Filterable;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.party.Party;
import play.twirl.api.Html;

import java.util.List;

/**
 * Widget shows list of users registered in play-cms
 */
public class RegisteredUsersWidget extends FrontendWidget {
    private static final String TAG = "registeredUsersWidget";
    private static final int MAX_USERS = 10;

    @Override
    public String[] filterTags() {
        return new String[] {TAG};
    }

    @Override
    public Html render(String tag, List<String> params, Filterable source) {
        List<Party> users = (List<Party>) PlayAuth.getPartyManager().findAll();
        int usersMaxSize = MAX_USERS;

        if (params.size() > 1) {
            try {
                usersMaxSize = Integer.parseInt(params.get(1));
            } catch (NumberFormatException e) {}
        }
        if (usersMaxSize < users.size()) {
            users = users.subList(0, usersMaxSize);
        }

        return widgets.registeredusers.html.registered_users.render(users);
    }
}

