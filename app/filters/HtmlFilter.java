package filters;

import ch.insign.commons.util.Configuration;
import com.google.inject.Inject;
import com.kenshoo.play.metrics.JavaMetricsFilter;
import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import com.mohiva.play.htmlcompressor.java.HTMLCompressorFilter;
import com.mohiva.play.htmlcompressor.java.HTMLCompressorBuilder;
import play.api.mvc.EssentialFilter;
import play.filters.gzip.GzipFilter;
import play.http.HttpFilters;

import java.util.ArrayList;

public class HtmlFilter implements HttpFilters {

    @Inject
    GzipFilter gzipFilter;

    /**
     * Use Metrics Filter, Gzip Filter, CustomHTMLCompressorFilter
     *
     * Metrics Filter - It records requests duration, number of active requests and counts each return code (see https://github.com/kenshoo/metrics-play )
     * GzipFilter - Provides a gzip filter that can be used to gzip responses (see https://www.playframework.com/documentation/2.4.x/GzipEncoding)
     * CustomHTMLCompressorFilter - Google's HTML (and XML) Compressor for Play Framework 2 (see https://github.com/mohiva/play-html-compressor)
     *
     * Configuration data you can find in application.conf file
     */
    @Override
    public EssentialFilter[] filters() {

        ArrayList filters = new ArrayList();

        if (Configuration.getOrElse("filter.gzip", true)) {
            filters.add(gzipFilter);
        }

        if (Configuration.getOrElse("filter.metrics", true)) {
            filters.add(new JavaMetricsFilter());
        }

        if (Configuration.getOrElse("filter.htmlCompressor", true)) {
            filters.add(new CustomHTMLCompressorFilter());
        }

        return (EssentialFilter[]) filters.toArray(new EssentialFilter[filters.size()]);
    }

    public class CustomHTMLCompressorFilter extends HTMLCompressorFilter {
        public CustomHTMLCompressorFilter() {
            super(new CustomHTMLCompressorBuilder());
        }
    }

    public class CustomHTMLCompressorBuilder implements HTMLCompressorBuilder {
        public HtmlCompressor build() {
            HtmlCompressor compressor = new HtmlCompressor();
            compressor.setPreserveLineBreaks(true);
            compressor.setRemoveComments(true);
            compressor.setRemoveIntertagSpaces(false);
            compressor.setRemoveHttpProtocol(false);
            return compressor;
        }
    }
}
