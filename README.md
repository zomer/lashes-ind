# Play-CMS Demo #

This is a Play framework demo application built with [insign](http://www.insign.ch)'s **play-cms** - a java-based Play framework CMS.

This demo creates some demo content and uses the h2 in-memory db for storage. It is intended to be used as quick-and-easy starting point for your own play-cms project. We've also added a docker-based devops setup for a CI (continuous integration) process.

Check out the [insign play-cms](https://confluence.insign.ch/display/PLAY) documentation for more information or visit the [play-cms community](https://plus.google.com/u/0/communities/100224277865065215900/)


## Quick start

Either do a local installation (if you have the play framework installed) using H2 by default, or run it in Docker.

### Local installation ###

Make sure you have the play framework 2.4+ installed. Checkout the project and run

    sbt run

By default application will be running on http://127.0.0.1:9000/ and uses a H2 in-memory db for (non-persistent) storage.

### Running in docker container ###
If you want to run play-cms-demo in a docker container, create a docker image with

    sbt docker:publishLocal

command, or pull the ready image from bintray

    docker pull insign-docker-registry.bintray.io/play-cms-demo:1.0

After that run the application with

    docker run -p 9000:9000 insign-docker-registry.bintray.io/play-cms-demo:1.0

By default, the container will be running on http://127.0.0.1:9000/ 

### Backend Access ###
Default admin page url: http://127.0.0.1:9000/admin

### Admin's credentials ###
login: admin@insign.ch

password: 123456

## Development environment and Continuous Integration ##
The project contains both a development environment and continuous integration setup (based on docker) for quick-starting new projects. 

### Use the development environment ###

Make sure you have docker and docker-compose installed.

Edit application.conf and comment the h2 section, uncomment the mysql section to use the MySQL db container.

Run 
```
devops/start.sh
```

This will build, launch and link the required containers and mount your code folder using docker-compose:

* App container, with mounted project code folder
* MySQL container and play db
* PhpMyAdmin container at localhost:1234 (user: playcmsdemo, pw: admin)

(For more details, check out devops/docker-compose.yml and common.yml)

To connect to your app container:

```
docker exec -it devops_app_1 bash
```

The project code folder is mounted. Use the usual activator or sbt commands to work with your code.


### CI setup with Jenkins ###
TODO

## Developing the demo project itself ##

### Publishing modules to bintray ###
Modules can be published to bintray with

    sbt publish

Credentials will be asked on first time. 

Note: bintray doesn't support snapshots.

Current version of a module can be easily removed from bintray with

    sbt bintrayUnpublish

### Publishing docker image to bintray registry ###
Next command will upload a docker image of play-cms-demo to insign's bintray registry:

    sbt docker:publish