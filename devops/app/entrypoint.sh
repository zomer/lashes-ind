#!/usr/bin/env bash

set -e

if [ "$1" = 'sbt' ]; then
    usermod -u $USER_ID sbt
    chown -R sbt /usr/src/app/source
    chown -R sbt /home/sbt
    exec gosu sbt "$@"
fi

exec "$@"